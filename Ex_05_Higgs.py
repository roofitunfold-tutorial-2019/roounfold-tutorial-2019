#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This script allows the user to apply an unfolding method of choice 
#       to data and MC based on the Hgammagamma analysis. The goal of this 
#       example is to show the differences between the various unfolding 
#       methods and the appropriate ways of using them.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================

from helpers import *


def main():

  import ROOT

  # Get the data. Set the second argument to True to 
  # plot the signal extracting fits.
  histograms = fit_histograms("data/higgs.root",False)

  print("What now....?")

if __name__=="__main__":
  main()
