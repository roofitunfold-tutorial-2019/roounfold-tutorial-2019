#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This example shows a simple unfolding scenario. Truth data is generated
#       according to a bimodal p.d.f. and then simulates detector response on
#       these events in a naive way by applying a simple smearing and 
#       efficiency function. The resulting truth and reco level events are
#       then used to build a response matrix that can be used for unfolding.
#       The to-be unfolded data is simulated by Poisson smearing a disjoint
#       reco level dataset. This is then used to estimated the bias of 
#       the unfolding setup by throwing a choosen number of toys.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================



dict_data = {}        

# ======= Constants ======= #

# Kinematic ranges. (delta eta)
dict_data['xmin'] = -4         
dict_data['xmax'] = 4          
                      
# Smearing parameters.
dict_data['bias'] = 1.1
dict_data['sigma'] = 0.85

# Efficiency type
dict_data['eff'] = 'lin'
                         
# Bimodal assymmetry.
dict_data['frac'] = 0.4  
                                                  
# Overflow flag.         
overflow = True          
                                                        
# Number of events.            
dict_data['nevents'] = 5000    
                                       
# Constant background bin count        
dict_data['nbkg'] = 5 

# ======================== #



# ===== User-defined ===== #
                      
# Truth/Reco bin count.  
dict_data['tbins'] = 30  
dict_data['rbins'] = 30  
                               
# How much more MC do we have than data
dict_data['mcLumiFactor'] = 10         

# ======================== #


from helpers import *


def main(args):
  import ROOT

  # Prepare the histograms and response matrix.
  histograms = prepare_bimodal(dict_data)

  # Create a spectator object.
  spec = ROOT.RooUnfoldSpec("unfold","unfold",histograms["truth_train"],"obs_truth",histograms["reco_train"],"obs_reco",histograms["response"],0, histograms["data"],overflow,0.0005)

  # Get the unfolding algorithm.
  alg = algorithm(args.method)

  # Get the bias calculatino method.
  bias_method = bias(args.biasmethod)

  if not args.regparm == None:
    func = spec.makeFunc(alg, args.regparm)
  else:
    func = spec.makeFunc(alg)

  unfold = func.unfolding()

  # Calculate the bias with toys.
  func.unfolding().CalculateBias(bias_method, args.ntoys)

  # Get the bias histogram.
  biashist = ROOT.RooUnfolding.convertTH1(unfold.Vbias(),unfold.Ebias(),unfold.response().Htruth())

  # Plot the bias histogram.
  plot_bias(biashist, histograms["truth_test"], func, True)


if __name__=="__main__":
  from argparse import ArgumentParser
  parser = ArgumentParser(description="RooUnfold testing script")
  parser.add_argument("method",default="inv",type=str)
  parser.add_argument("--regparm",type=float)
  parser.add_argument("--biasmethod",type=str)
  parser.add_argument("--ntoys",default=100,type=int)
  main(parser.parse_args())
  
