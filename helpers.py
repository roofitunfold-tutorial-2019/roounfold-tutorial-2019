#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This script contains helper functions for the exercise scripts.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================


import math
import numpy
import os

if not os.path.isdir('./output'):
  os.mkdir('./output')

# ====== Efficiencies ====== #

# A quadratic efficiency.
def quad_eff(xt, xmin, xmax):

  diff = xmax - xmin

  center = xmin + diff/2

  xeff = 0.1 + 0.7 * (1 - ((center - xt)/diff)**2 )

  return xeff

# A linear efficiency.
def lin_eff(xt, xmin, xmax):

  xeff = 0.3 + (1.0-0.3)/(xmax - xmin)*(xt - xmin)

  return xeff



# ====== Smearing ====== #

def smear(xt, xmin, xmax, bias, sigma, eff):

  from ROOT import gRandom

  if eff == 'lin':
    xeff = lin_eff(xt, xmin, xmax)
  elif eff == 'quad':
    xeff = quad_eff(xt, xmin, xmax)
  else:
    print("Unknown passed efficiency type. Pass either lin or quad.")
    exit(0)

  x= gRandom.Rndm();

  if x>xeff: return None;

  xsmear= gRandom.Gaus(bias,sigma);     #  bias and smear
  
  return xt+xsmear




# ====== Plotting ====== #


def plot_input(truth, reco, data, response, purity, eff):

  import ROOT

  c = ROOT.TCanvas("input","input", 900, 500)
  c.Divide(2,2)
  
  c.cd(1)

  ROOT.gStyle.SetOptStat(0)

  truth.SetLineColor(46)
  truth.GetYaxis().SetRangeUser(0,1.2*truth.GetMaximum())
  truth.GetYaxis().SetTitle("Events")
  truth.GetXaxis().SetTitle("#Delta#eta")
  truth.SetTitle("Input distributions")
  truth.Draw("HIST")

  reco.SetLineColor(38)
  reco.SetTitle("")
  reco.Draw("HISTSAME")

  data.SetMarkerColor(ROOT.kBlack)  
  data.SetLineColor(ROOT.kBlack)  
  data.SetMarkerStyle(20)
  data.SetMarkerSize(0.5)
  data.SetTitle("")
  data.Draw("SAMEPE")

  leg = ROOT.TLegend(0.65, 0.65, 0.85, 0.85)
  leg.SetLineWidth(0)
  leg.SetFillStyle(0)
  leg.AddEntry( truth, "Truth MC", "l" )
  leg.AddEntry( reco, "Reco MC", "l" )
  leg.AddEntry( data, "Measured Data", "pe" )
  leg.Draw()
  
  c.cd(2)

  ROOT.gStyle.SetOptStat(0)

  response.GetXaxis().SetTitle("#Delta#eta_{reco}")
  response.GetYaxis().SetTitle("#Delta#eta_{truth}")
  response.SetTitle("Response matrix")
  response.Draw("COLZ")
  c.cd(3)
  
  ROOT.gStyle.SetOptStat(0)
  
  purity.SetLineColor(30)
  purity.GetYaxis().SetRangeUser(0,1.2*purity.GetMaximum())
  purity.GetYaxis().SetTitle("Purity")
  purity.GetXaxis().SetTitle("#Delta#eta_{reco}")
  purity.SetTitle("Bin purity")
  purity.Draw()

  c.cd(4)

  ROOT.gStyle.SetOptStat(0)
  
  eff.SetLineColor(30)
  eff.GetYaxis().SetRangeUser(0,1.2*eff.GetMaximum())
  eff.GetYaxis().SetTitle("Efficiency")
  eff.GetXaxis().SetTitle("#Delta#eta_{truth}")
  eff.SetTitle("Reconstruction efficiency")
  eff.Draw()
  
  c.SaveAs("output/input.png")



def plot_result(theory, unfold_func, errors):

  import ROOT

  print('Plotting unfolding results...')

  truth_obs = unfold_func.unfolding().response().Htruth().obs(0)
  frame = truth_obs.frame()

  frame.GetYaxis().SetTitle("events")

  canvas_truth = ROOT.TCanvas("result","result")

  ROOT.gPad.SetLeftMargin(0.15)
 

  if errors:
    unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20),ROOT.RooFit.VisualizeError(ROOT.RooFitResult.prefitResult(unfold_func.makeParameterList())))
  else:
    unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20))

  frame.Draw()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)  
  theory.SetLineColor(ROOT.kRed)
  theory.Draw("HISTSAME")
  leg_truth = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
  leg_truth.SetLineWidth(0)
  leg_truth.SetFillStyle(0)
  leg_truth.AddEntry( theory, "Truth MC", "l" )
  leg_truth.AddEntry( frame.findObject("plot_unfolded"), "Unfolded Data", "pe" )
  leg_truth.Draw()
  canvas_truth.SaveAs("output/result.png")


def plot_result_multi(histograms, unfolded_funcs, regs, errors):
  
  print('Plotting unfolding results...')

  import ROOT
  from ROOT import gPad

  canvas_dim = int(math.ceil(math.sqrt(len(unfolded_funcs))))

  canvas = ROOT.TCanvas("result","result")

  canvas.Divide(canvas_dim, canvas_dim)  

  
  leg_truth = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
  leg_truth.SetLineWidth(0)
  leg_truth.SetFillStyle(0)

  for i in range(len(unfolded_funcs)):
    
    canvas.cd(i + 1)

    unfold_func = unfolded_funcs[i]
    theory = histograms["truth_test"]

    truth_obs = unfold_func.unfolding().response().Htruth().obs(0)
    frame = truth_obs.frame()

    frame.GetYaxis().SetTitle("events")

    if errors:
      unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20),ROOT.RooFit.MarkerSize(0.5),ROOT.RooFit.VisualizeError(ROOT.RooFitResult.prefitResult(unfold_func.makeParameterList())))
    else:
      unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20),ROOT.RooFit.MarkerSize(0.5))

    frame.Draw()
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetOptTitle(0)  
    theory.SetLineColor(ROOT.kRed)
    theory.Draw("HISTSAME")

    if i == 0:
      leg_truth.AddEntry( theory, "Truth MC", "l" )
      leg_truth.AddEntry( frame.findObject("plot_unfolded"), "Unfolded Data", "pe" )
        
    leg_truth.Draw("SAME")
    gPad.Update()
    ROOT.gDirectory.Clear()
  
  canvas.SaveAs("output/result_multi.png")


def plot_bias(bias, theory, unfold_func, errors):

  print('Plotting bias results...')

  import ROOT
  
  canvas = ROOT.TCanvas("Bias","Bias",1000,400)
    
  canvas.Divide(2,1)
  
  ROOT.gStyle.SetOptStat(0)

  canvas.cd(1)

  truth_obs = unfold_func.unfolding().response().Htruth().obs(0)
  frame = truth_obs.frame()
  
  frame.GetYaxis().SetTitle("events")

  if errors:
    unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20),ROOT.RooFit.VisualizeError(ROOT.RooFitResult.prefitResult(unfold_func.makeParameterList())))
  else:
    unfold_func.plotOn(frame,ROOT.RooFit.DrawOption("P"),ROOT.RooFit.Name("plot_unfolded"),ROOT.RooFit.MarkerStyle(20))

  frame.Draw()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gStyle.SetOptTitle(0)  
  theory.SetLineColor(ROOT.kRed)
  theory.Draw("HISTSAME")
  leg_truth = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
  leg_truth.SetLineWidth(0)
  leg_truth.SetFillStyle(0)
  leg_truth.AddEntry( theory, "Truth MC", "l" )
  leg_truth.AddEntry( frame.findObject("plot_unfolded"), "Unfolded Data", "pe" )
  leg_truth.Draw()


  canvas.cd(2)

  bias.SetMarkerStyle(20)
  bias.SetMarkerSize(0.5)
  bias.SetTitle("Bias")
  bias.GetXaxis().SetTitle("#Delta#eta_{truth}")
#  bias.GetYaxis().SetRangeUser(-5,5)
  bias.Draw("PE")
  line = ROOT.TLine(truth_obs.getBinning().lowBound(),0,truth_obs.getBinning().highBound(),0);
  line.SetLineStyle(7)
  line.Draw("SAME");  
  

  canvas.SaveAs("output/bias.png")




# ======= Data generation ======= #

def prepare_bimodal(dict_data):

  print("Generating the data...")

  import ROOT

  # Get the parameters.
  xmin = dict_data['xmin']
  xmax = dict_data['xmax']
  recoBins = dict_data['rbins']
  truthBins = dict_data['tbins']
  bias = dict_data['bias']
  sigma = dict_data['sigma']
  frac = dict_data['frac']
  nevents = dict_data['nevents']
  bkg_events = dict_data['nbkg']
  mcLumiFactor = dict_data['mcLumiFactor']
  eff_type = dict_data['eff']

  # Define the response matrix as a 2D histogram.
  response= ROOT.RooUnfoldResponse (recoBins, xmin, xmax, truthBins, xmin, xmax);  

  # Define the truth histogram that will be used for testing.
  truth = ROOT.TH1D ("Truth", "Truth", truthBins, xmin, xmax);

  # Define the reco histogram to compare to the data.
  reco = ROOT.TH1D ("Reco", "Reco", recoBins, xmin, xmax);
  
  # Define the data histogram.
  data = ROOT.TH1D ("Data", "Data", recoBins, xmin, xmax);

  for i in range(1,truth.GetNbinsX() + 1):
    truth.SetBinContent(i, bkg_events)
  for i in range(reco.GetNbinsX() + 2):
    reco.SetBinContent(i, bkg_events)
    data.SetBinContent(i, bkg_events)

  # Generate events according too a simple double Gaussian
  # distribution.
  for i in range(nevents*mcLumiFactor):

    if i%2 == 0:
      peak_loc = 0.3
    else:
      peak_loc = 0.7

    x_truth = ROOT.gRandom.Gaus(xmin + (xmax-xmin)*peak_loc, (xmax-xmin)*0.1)

    # Use a specific smearing function that also models
    # detector inefficiencies. 
    x_reco = smear(x_truth, xmin, xmax, bias, sigma, eff_type)

    # The truth event is reconstructed.
    if x_reco!=None:
      response.Fill (x_reco, x_truth, 1./mcLumiFactor);

    # The truth event did not pass inefficiencies.
    else:
      response.Miss (x_truth, 1./mcLumiFactor);

    # Fill the testing histograms with seperate events.
    x_truth = ROOT.gRandom.Gaus(xmin + (xmax-xmin)*peak_loc, (xmax-xmin)*0.1)
    x_reco = smear(x_truth, xmin, xmax, bias, sigma, eff_type)
 
    if x_reco!=None: reco.Fill(x_reco, 1./mcLumiFactor)      

    truth.Fill(x_truth, 1./mcLumiFactor)

    # Introduce a shape difference for the data.
    if i > int(frac*nevents*mcLumiFactor):
      peak_loc = 0.3
    else:
      peak_loc = 0.7

    x_truth = ROOT.gRandom.Gaus(xmin + (xmax-xmin)*peak_loc, (xmax-xmin)*0.1)
    x_data = smear(x_truth, xmin, xmax, bias, sigma, eff_type)
    
    if x_data!=None: data.Fill(x_data, 1./mcLumiFactor)

     
  # Create Poisson distributed observed data events.
  for i in range(data.GetNbinsX() + 2):
    x_data = ROOT.gRandom.Poisson(data.GetBinContent(i))
    data.SetBinContent(i, x_data)

  eff = ROOT.RooUnfolding.convertTH1(response.Vefficiency(),response.Htruth())
  pur = ROOT.RooUnfolding.convertTH1(response.Vpurity(),response.Htruth())  
    
  # Save the histograms in a dictionary.
  histograms = {"truth_train":response.Htruth(),
                "reco_train":response.Hmeasured(),
                "truth_test":truth,
                "reco_test":reco,
                "data":data,
                "response":response.Hresponse(),
                "purity":pur,
                "eff":eff
              }
  
  
  for i in range(0,histograms["response"].GetNbinsX()+2):
    for j in range(0,histograms["response"].GetNbinsY()+2):
      histograms["response"].SetBinError(i,j,0)

  return histograms



def get_histograms(file_path):

  import ROOT

  inputFile = ROOT.TFile.Open(file_path,"READ")
  if not inputFile or not inputFile.IsOpen():
    raise RuntimeError("unable to open input file 'data/histograms_sys.root'")
  
  truth_nom = inputFile.Get("Nom/truth")
  reco_nom = inputFile.Get("Nom/reco")
  response_nom = inputFile.Get("Nom/response")
  truth_test = inputFile.Get("Nom/truth_test")
  data = inputFile.Get("Nom/data")
  
  truth_up = inputFile.Get("NP1/truth/up")
  truth_down = inputFile.Get("NP1/truth/down")

  response_upnom = inputFile.Get("NP1/response/up")
  response_downnom = inputFile.Get("NP1/response/down")

  reco_up = inputFile.Get("NP2/reco/up")
  reco_down = inputFile.Get("NP2/reco/down")

  response_nomup = inputFile.Get("NP2/response/up")
  response_nomdown = inputFile.Get("NP2/response/down")


  truth_nom.SetDirectory(0)
  reco_nom.SetDirectory(0)
  response_nom.SetDirectory(0)
  truth_test.SetDirectory(0)
  data.SetDirectory(0)
  
  truth_up.SetDirectory(0)
  truth_down.SetDirectory(0)
  
  response_upnom.SetDirectory(0)
  response_downnom.SetDirectory(0)
  
  reco_up.SetDirectory(0)
  reco_down.SetDirectory(0)

  response_nomup.SetDirectory(0)
  response_nomdown.SetDirectory(0)

  histograms = {"truth_nom":truth_nom,
                "reco_nom":reco_nom,
                "response_nom":response_nom,
                "data":data,
                "truth_test":truth_test,
                "truth_up":truth_up,
                "truth_down":truth_down,
                "response_upnom":response_upnom,
                "response_downnom":response_downnom,
                "reco_up":reco_up,
                "reco_down":reco_down,
                "response_nomup":response_nomup,
                "response_nomdown":response_nomdown
              }

  inputFile.Close()

  return histograms


def fit_histograms(data_set, plot_flag):

  import ROOT

  # Set the bin boundaries.
  xbins = numpy.array((0.,5.,10.,15.,20.,25.,30.,35.,45.,60.,80.,100.,120.,140.,170.,200.,250.,350.))
  
  # Set the bin widths.
  binWidths = numpy.diff(xbins)

  # Define the histograms.
  data = ROOT.TH1D("ptsignal","ptsignal",17, xbins)
  truth = ROOT.TH1D("truth","truth",17,xbins)
  reco = ROOT.TH1D("reco","reco",17,xbins)
  response = ROOT.TH2D("resp","resp",17,xbins,17,xbins)

  # Get the hyy data file.
  inputFile = ROOT.TFile(data_set,"READ")

  # Create the variable of the to be fitted histograms.
  myy = ROOT.RooRealVar("m_yy_reco","m_yy_reco",105,160)

  # Set a variety of ranges for different parts of the fit.
  myy.setRange("low",105,120)
  myy.setRange("high",130,160)
  myy.setRange("signal",122,128)
  myy.setRange("full",105,160)


  # Loop over all the myy histograms corresponding
  # to a pt bin.
  for pt_bin in range(18):

    dir_name = "data_bin"

    dir_name = dir_name+str(pt_bin)

    # Get the tree with the data.
    tree = inputFile.Get(dir_name)
    
    # Create the dataset.
    fit_data = ROOT.RooDataSet(dir_name,dir_name,tree,ROOT.RooArgSet(myy))

    # Create the parameters for the Gaussian signal model.
    mu = ROOT.RooRealVar("mu{}".format(pt_bin),"mu",125,123,127)
    sigma = ROOT.RooRealVar("sigma{}".format(pt_bin),"sigma",2,0.5,5)
  
    # Create the Gaussian signal model.
    signal = ROOT.RooGaussian("s","s",myy,mu,sigma)
    
    # Create the parameter for the exponential background model.
    lam = ROOT.RooRealVar("lambda{}".format(pt_bin),"lambda",-.035,-10,0)

    # Create the exponential background model.
    bkg = ROOT.RooExponential("b{}".format(pt_bin),"b",myy,lam)

    # Create the normalization parameters for both the signal and
    # background model that will represent the total number of
    # signal and background events.
    nsig = ROOT.RooRealVar("ns{}".format(pt_bin),"nsig",10,0,1000)
    nbkg = ROOT.RooRealVar("nb{}".format(pt_bin),"nbkg",1000,100,100000)
    
    # Combine the background model and the normalization parameter.
    ebkg = ROOT.RooExtendPdf("ebkg","ebkg",bkg,nbkg,"full")
    
    # Fit the sidebands of the histogram to the background model to
    # extract the total number of background events.
    ebkg.fitTo(fit_data, ROOT.RooFit.Range("low,high"),ROOT.RooFit.Extended(1))

    # Set the fitted background events constant for future fits.
    nbkg.setConstant(ROOT.kTRUE)
    
    # Only the background parameter lambda is floating. Fit again
    # to extract the correct background shape.
    bkg.fitTo(fit_data,ROOT.RooFit.Range("low,high"))

    # Set the fitted background shape constant.
    lam.setConstant(ROOT.kTRUE)    

    # Combine the signal model and the normalization parameter.
    esig = ROOT.RooExtendPdf("esig","esig",signal,nsig,"full")

    # Build the composite signal and background model by summing the two.
    model = ROOT.RooAddPdf("model","model",ROOT.RooArgList(esig,ebkg))

    # Fit the model to the signal shape (mu, sigma)
    model.fitTo(fit_data,ROOT.RooFit.Range("full"),ROOT.RooFit.Extended(1))

    # Set the fitted Gaussian signal model parameters constant.
    mu.setConstant(1)
    sigma.setConstant(1)

    # Fit the total number of signal events as final POI.
    model.fitTo(fit_data,ROOT.RooFit.Range("full"),ROOT.RooFit.Extended(1))

    
    # Last fitted value is the overflow bin.
    if pt_bin == 17:
      data.SetBinContent(pt_bin + 1, nsig.getVal() )
      data.SetBinError(pt_bin+1, nsig.errorVar().getVal())
    else:
      data.SetBinContent(pt_bin + 1, nsig.getVal() / binWidths[pt_bin] )
      data.SetBinError(pt_bin+1, nsig.errorVar().getVal() / binWidths[pt_bin])

    if plot_flag:
      frame = myy.frame()
      
      frame.SetTitle("pt bin {}".format(pt_bin+1))
      frame.GetXaxis().SetTitle("m_{#gamma#gamma}")
      frame.GetYaxis().SetTitle("events")
      
      canvas = ROOT.TCanvas("result","result",500,400)
      
      fit_data.plotOn(frame,ROOT.RooFit.Name("data"),ROOT.RooFit.MarkerStyle(20))
      model.plotOn(frame,ROOT.RooFit.Name("sigbkg"),ROOT.RooFit.Components("model"),ROOT.RooFit.LineColor(2))
      model.plotOn(frame,ROOT.RooFit.Name("bkg"),ROOT.RooFit.Components("ebkg"),ROOT.RooFit.LineColor(2),ROOT.RooFit.LineStyle(7))
      
      frame.Draw()
      
      leg_truth = ROOT.TLegend(0.7, 0.7, 0.9, 0.9)
      leg_truth.SetLineWidth(0)
      leg_truth.SetFillStyle(0)
      leg_truth.AddEntry( frame.findObject("sigbkg"), "Sig + Bkg", "l" )
      leg_truth.AddEntry( frame.findObject("bkg"), "Bkg", "l" )
      leg_truth.AddEntry( frame.findObject("data"), "Data", "pe")
      leg_truth.Draw()

      canvas.SaveAs("output/myyfit_ptbin_{}.png".format(pt_bin))


  # Get the tree with the truth and reco events.
  tree = inputFile.Get("response")
  
  # Get the events.
  nEntries = tree.GetEntries()

  # Loop over the events.
  for i in range(nEntries):
    
    tree.GetEntry(i)
    x_reco = tree.reco
    x_truth = tree.true

    # Fill the histograms. Because of varying bin width the histograms
    # need to be filled and then used as input for the RooUnfoldResponse 
    # object instead of filling the RooUnfoldResponse object directly.
    if x_reco > 0:
      response.Fill(x_reco, x_truth)
      truth.Fill(x_truth)
      reco.Fill(x_reco)
    else:
      truth.Fill(x_truth)
  
  # Set the underflow bin.
  data.SetBinContent(0, 0)
  data.SetBinError(0, 0)

  # Build the response matrix.
  responseObject = ROOT.RooUnfoldResponse (reco,truth,response)

  # Save the histograms in a dictionary.
  histograms = {"truth_train":responseObject.Htruth(),
                "reco_train":responseObject.Hmeasured(),
                "data":data,
                "response":responseObject.Hresponse()
                }

  for i in range(0,histograms["response"].GetNbinsX()+2):
    for j in range(0,histograms["response"].GetNbinsY()+2):
      histograms["response"].SetBinError(i,j,0)

  inputFile.Close()

  return histograms

# ======= Parsing ======= #

# Bias calculation methods.

def bias(method):

  import ROOT

  bias_type = None
  if method == "est":
    bias_type= ROOT.RooUnfolding.kBiasEstimator;
  elif method == "clos":
    bias_type= ROOT.RooUnfolding.kBiasClosure;
  elif method == "asi":
    bias_type= ROOT.RooUnfolding.kBiasAsimov;
  else:
    print("The passed bias calculation method does not match any of the supported methods. Please pass one of the following methods:")
    print("est")
    print("clos")
    print("asi")
    exit(0)
  return bias_type

# Unfolding methods.
def algorithm(method):

  import ROOT

  alg = None
  if method == "bayes":
    alg= ROOT.RooUnfolding.kBayes;
  elif method == "bbb":
    alg= ROOT.RooUnfolding.kBinByBin;
  elif method == "inv":
    alg= ROOT.RooUnfolding.kInvert;
  elif method == "svd":
    alg= ROOT.RooUnfolding.kSVD;
  elif method == "root":
    alg= ROOT.RooUnfolding.kTUnfold;
  elif method == "ids":
    alg= ROOT.RooUnfolding.kIDS;
  elif method == "gp":
    alg= ROOT.RooUnfolding.kGP;
  else:
    print("The passed unfolding method does not match any of the supported methods. Please pass one of the following methods:")
    print("bayes")
    print("bbb")
    print("inv")
    print("svd")
    print("root")
    print("ids")
    print("gp")
    exit(0)

  return alg
